#!/usr/bin/env python
# -*- coding: utf-8 -*-

# sn2email
#
# Copyright (C) 2012-2013 Simó Albert i Beltran
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import sys
import os
import time
import urllib
import httplib2
import smtplib
import json
import string

status_maxlength = '140'

file_db = 'sn2email.db'

def get_url(url, auth = True, post_data = None):
	if post_data:
		headers = { 'Content-type': 'application/x-www-form-urlencoded' }

	request = httplib2.Http()
	if auth:
		request.add_credentials(db_get('user'), db_get('password'))
	try:
		if post_data:
			response, content = request.request(url, 'POST', post_data, headers)
		else:
			response, content = request.request(url)
	except:
		return
	else:
		if response['status'] == '200':
			return content
		else:
			print "ERROR getting " + url
			print response
			print content

def get_data(url, post_data = None):
	data = get_url(url, True, post_data)
	if data:
		data = json.loads(data)
		if 'error' in data:
			print json.dumps(data,indent=2)
			raise
		else:
			return data

def get_user_timeline():
	sleep = db_get('last_fetch') + 24 - time.time()
	if sleep > 0:
		time.sleep(sleep)
	values = {
		'include_rts': 'true',
		'since_id': db_get('last_status')
	}
	params = urllib.urlencode(values)
	db_lock()
	db_set('last_fetch', time.time())
	db_lock(False)
	timeline = get_data(db_get('api') + '/statuses/home_timeline.json?' + params)
	if timeline:
		timeline.reverse()
		return timeline

def db_lock(lock = True):
	db = db_read()
	if lock:
		while db['lock']:
			time.sleep(1)
			db = db_read()
	db['lock'] = lock
	db_save(db)

def db_get(key):
	db = db_read()
	return db[key]

def db_set(key, value):
	db = db_read()
	db[key] = value
	db_save(db)

def db_read():
	save = False
	try:
		f = open(file_db)
		db = json.loads(f.read())
	except:
		db = {
			'lock': False,
#			'url': 'https://example.com',
#			'api': 'https://example.com/api',
#			'user': 'user',
#			'password': 'pass',
			'mailserver': 'localhost',
#			'sender': 'sender@example.com',
#			'recipient': 'recipient@example.com',
			'last_fetch': 0,
			'last_status': 1,
			'status': {},
			'post': {}
		}
		save = True
	else:
		# update database 2014-06-10
		if not 'lock' in db:
			db['lock'] = False
			save = True
		# update database 2014-06-17
		if not 'post' in db:
			db['post'] = {}
			save = True
	finally:
		f.close()
		if save:
			db_save(db)
		return db

def config():
	if sys.argv[1] == 'config':
		db_lock()
		db_set(sys.argv[2], sys.argv[3])
		db_lock(False)

def update():
	timeline = get_user_timeline()
	if timeline:
		for status in timeline:
			extract_retweet(status)

def extract_retweet(status):
	if 'retweeted_status' in status:
		extract_retweet(status['retweeted_status'])
	# Avoid Friendica bug "A status cannot be a reply of itself" https://github.com/friendica/friendica/issues/1010
	elif 'in_reply_to_status_id' in status and isinstance(status['in_reply_to_status_id'], (int, long)) and status['in_reply_to_status_id'] != status['id']:
		status_in_reply = fetch_status(status['in_reply_to_status_id'])
		if status_in_reply:
			extract_retweet(status_in_reply)
		else:
			db_lock()
			db_status = db_get('status')
			if not status['in_reply_to_status_id'] in db_status:
				db_status[status['in_reply_to_status_id']] = 'pending'
				db_set('status',db_status)
			db_lock(False)
	return send(status)

def fetch_pending_statuses():
	db_status = db_get('status')
	for status_id in db_status:
		if db_status[status_id] == 'pending':
			pending_status = fetch_status(status_id)
			if pending_status:
				extract_retweet(pending_status)

def send(status):
	db_lock()
	db_status = db_get('status')
	if not str(status['id']) in db_status:
		if 'retweeted_status' in status:
			new_status = send_retweet(status)
		else:
			new_status = send_status(status)
		db_status[str(status['id'])] = 'sent'
		db_set('status', db_status)
		db_set('last_status', status['id'])
	db_lock(False)

def get_in_reply_to(status):
	if 'in_reply_to_status_id' in status and isinstance(status['in_reply_to_status_id'], (int, long)):
		return '<' + str(status['in_reply_to_status_id']) + '@sn2email>'
	else:
		return None

def send_retweet(status):
	url = db_get('url')
	text = status['user']['screen_name'] + ': retweet ' + url + '/notice/' + str(status['id'])
	mail(status['id'], status['user']['screen_name'], url, text, 'text/plain', '<' + str(status['retweeted_status']['id']) + '@sn2email>', status['created_at'])
	#origin_status_id = str(status['retweeted_status']['id'])
	#print "RT by " + status['user']['screen_name'] + " of " + origin_status_id

def send_status(status):
	print json.dumps(status,indent=2)
	url = db_get('url')
	url_status = url + '/notice/' + str(status['id'])
	url_context = url + '/conversation/' + str(status['statusnet_conversation_id']) + '#notice-' + str(status['id'])
	url_timeline = url + '/' + db_get('user') + '/all'
	api = db_get('api')
	url_retweet = api + '/statuses/retweet/' + str(status['id']) + '.xml'
	url_favorite = api + '/favorites/create/' + str(status['id']) + '.xml'
	url_reply = api + '/statuses/update.xml'
	type = db_get('type')
	if type == 'text' and 'text' in status:
		mime = 'text/plain'
		text =  status['user']['screen_name'] + ':\n ' + status['text'] + '\n\n--\n' + url_status + '\n' + url_timeline
	if type == 'html' and 'statusnet_html' in status:
		mime = 'text/html'
		header_html = '\n<a href="' + status['user']['statusnet_profile_url'] +'">' + status['user']['screen_name'] + '</a>: \n'
		footer_html = '\n<br/>--<br/><a href="' + url_status + '">status</a>'
		footer_html = footer_html + '\n<br/><a href="' + url_context + '">context</a>'
		footer_html = footer_html + '\n<br/><a href="' + url_timeline + '">timeline</a>'
		footer_html = footer_html + '\n<br/><form method="post" action="' + url_reply + '"><input type="hidden" name="source" value="sn2email" /><input type="hidden" name="in_reply_to_status_id" value="' + str(status['id']) + '"><textarea name="status" maxlength="' + status_maxlength + '"></textarea><input type="submit" value="reply" /></form>'
		footer_html = footer_html + '\n <form method="post" action="' + url_retweet + '"><input type="hidden" name="source" value="sn2email" /><input type="submit" value="retweet" /></form>'
		footer_html = footer_html + '\n <form method="post" action="' + url_favorite + '"><input type="hidden" name="source" value="sn2email" /><input type="submit" value="favorite" /></form>'
		text =  '<html><body>' + header_html + (status['statusnet_html']).encode('ascii', 'xmlcharrefreplace') + footer_html + '</body></html>'
	if 'attachments' in status:
		if len(status['attachments']) > 0:
			if status['attachments'][0]['mimetype'] == 'text/html':
				mime = 'text/html'
				data = get_url(status['attachments'][0]['url'], False)
				if data:
					print data
					text = header_html + data.decode('utf-8').encode('ascii', 'xmlcharrefreplace') + footer_html
#	if not text:
#		mime = 'text/plain'
#		text =  status['text']
#	text = unicode(text).encode('utf-8').decode('utf-8')
	if status['truncated']:
		text = text + ' (truncated)'
	mail(status['id'], status['user']['screen_name'], url, text, mime, get_in_reply_to(status), status['created_at'])
	#user = status['user']['screen_name']
	#print user + ":"
	#print status['text']
	#print url + '/notice/' + str(status['id'])
	if isinstance(status['in_reply_to_status_id'], (int, long)):
		print "Reply of " + str(status['in_reply_to_status_id'])
#		status = fetch_status(status['in_reply_to_status_id'])
#		if status:
#			send(status)

def mail(id, name, subject, text, mime, in_reply_to = None, date = None):
	sender = db_get('sender')
	recipient = db_get('recipient')
	msg = \
		'From: "' + name + '" <' + sender + '>\r\n' + \
		'To: ' + recipient + '\r\n' + \
		'List-Id: <sn2email.localhost> \r\n' + \
		'Message-ID: <' + str(id) + '@sn2email>\r\n' + \
		'Subject: ' + subject + '\r\n' + \
		'Content-type: ' + mime + '; charset=UTF-8\r\n'
	if in_reply_to:
		msg = msg + 'In-Reply-To: ' + str(in_reply_to) + '\r\n'
	if date:
		msg += 'Date: ' + date + '\r\n'
#	msg = msg.encode('utf-8') + '\r\n' + \
#		user.encode('utf-8') + ':\r\n' + \
#		text.encode('utf-8') + '\r\n' + \
#		(db_get('url')).encode('utf-8') + '/notice/' + str(id).encode('utf-8') + '\r\n'

	msg = msg + '\r\n'
	#msg = msg.encode('utf-8') + text + '\r\n'
	msg = msg + text + '\r\n'
	#msg = msg + url.encode('utf-8') + ('\r\n').encode('utf-8')
	#msg = msg + url + '\r\n'

	server = smtplib.SMTP(db_get('mailserver'))
	server.sendmail(sender, recipient, msg.encode('utf-8'))
	server.quit()

def fetch_status(status_id):
	return get_data(db_get('api') + '/statuses/show/' + str(status_id) + '.json')

def help():
	print 'Usage:'
	print ' Run:'
	print '  ' + sys.argv[0]
	print ' Config:'
	print '  ' + sys.argv[0] + ' config <item> <value>'

def db_save(db):
	f = open(file_db, "w")
	#f.write(json.dumps(db))
	f.write(json.dumps(db, indent=2))
	f.close()

def check_configuration():
	db = db_read()
	configured = True
	if not 'api' in db:
		print 'No "api" configured. Example: https://example.com/api'
		configured = False
	if not 'url' in db:
		print 'No "url" configured. Example: https://example.com'
		configured = False
	if not 'user' in db:
		print 'No user configured.'
		configured = False
	if not 'password' in db:
		print 'No password configured.'
		configured = False
	if not 'sender' in db:
		print 'No sender email address configured.'
		configured = False
	if not 'recipient' in db:
		print 'No recipient email address configured.'
		configured = False
	if not 'type' in db:
		print 'No "type" configured. Values: html or text.'
		configured = False
	return configured

def get_message_body(message):
	for part in message.walk():
		if part.get_content_type() == 'text/plain':
			payload = part.get_payload()
			content_transfer_encoding = part.get('Content-Transfer-Encoding')
			if content_transfer_encoding == 'quoted-printable':
				import quopri
				payload = quopri.decodestring(payload)
			elif content_transfer_encoding == 'base64':
				payload = payload.decode('base64')
			return payload

def post(message_id):
	db_lock()
	db_post = db_get('post')
	values = db_post[message_id]
	id = str(time.time())
	if 'in_reply_to_status_id' in values:
		id += 'processed' + values['in_reply_to_status_id']

	params = urllib.urlencode(values)
	response = get_data(db_get('api') + '/statuses/update.json', params)

	if response:
		mail(id, 'sn2email', 'processed ' + message_id, json.dumps(values,indent=2) + '\r\n' + json.dumps(response,indent=2), 'text/plain', message_id)
		del db_post[message_id]
		db_set('post', db_post)
	db_lock(False)

def post_pending_statuses():
	db_post = db_get('post')
	for post_id in db_post:
		post(post_id)

def parse_email():
	import email
	message = email.message_from_file(sys.stdin)
	message_id = message.get('Message-ID')
	body = get_message_body(message)

	values = {
		'status': body,
		'source': 'sn2email'
	}

	in_reply_to = message.get('In-Reply-To')
	if in_reply_to:
		in_reply_to = in_reply_to.partition('<')[2].partition('@')[0]
		values['in_reply_to_status_id'] = in_reply_to

	db_lock()
	db_post = db_get('post')
	db_post[message_id] = values
	db_set('post', db_post)
	db_lock(False)
	post(message_id)

if len(sys.argv) >= 3:
	config()
elif check_configuration():
	if len(sys.argv) == 2 and sys.argv[1] == 'email':
		parse_email()
	else:
		while True:
			update()
			fetch_pending_statuses()
			post_pending_statuses()
else:
	help()
